#####################
# CSU_Project
# Author: Noraim Nunez 
######################
# Part1 

#1. Select one analysis. 

.
#2. Identify the final state of your signal
    A. How many leptons do you expect? 
    B. How many jets? 
    C. Do you expect neutrinos or other source of missing transverse energy ? 

.
#3. What are other Standard Model processess share the same final state? List the background processes.
Identify the final state of your signal.

#4. Which samples from the list of datasents would you use? List them all and explain why. 



#####################
#Part2 

.
#1. Implement your own analysis class. Use ZPrimeAnalysis as an example. 

.
#2. Make histograms of the main kinematic variables for your signal and backgrounds 

.
#3. Decide on a list of event selection criteria based on the histograms of the kinematic variables

.
#4 Make a stack histograms of your kinematic variables and event selection 

#####################
#Part3

.
#1. Produce the expected and observed limit plots for your analysis.

