import ROOT
import ctypes 
import sys 
import numpy as np 
import numpy 
from ROOT import TCanvas, TBrowser, TFile, TPaveText, TPaveStats, TProfile, TNtuple, TH1F, TH2F, TLorentzVector, THStack, TLegend, TCut, TGraph, TPad
from ROOT import gROOT, gBenchmark, gSystem, gStyle, TGaxis, TTree 
from datetime import date 

#JSROOT implements user interface for THttpServer class 
%jsroot on

def analyze(datasample, aFile):
    
    d = date.today()                       
    tree = 'mini'
    rootFile = TFile(aFile) 
    tree_pull = rootFile.Get(tree)
    nEntries = tree_pull.GetEntries() 
    
    #Histograms in 1Dimension 
    #objects of interest from branch: mini
    lep_n =  TH1F("lep_n","Number of leptons        ;[GeV] ;Number of Events", 60,0,10)   
    lep_pt=  TH1F("lep_pt","Trans-Momentum of Leptons;[GeV] ;Number of Events", 200,0,200)                          #1
    lep_eta= TH1F("lep_eta","Angle of Leptons;[GeV] ;Number of Events",200,0,500)                         #2 Name in branch: photon_n 
    lep_phi= TH1F("lep_phi","   ;[GeV] ;Number of Events", 60,0,10000)                           #3 
    met_et = TH1F("met_et","     ;[GeV] ;Number of Events",60,0,5)        #4 Name in branch: photon_pt 
    met_phi= TH1F("met_phi","   ;[GeV] ;Number of Events", 200,0,10)       #5
    jet_n  = TH1F("jet_n","       ;[GeV] ;Number of Events",60,0,15) 
    jet_pt = TH1F("jet_pt","      ;[GeV] ;Number of Events",200,0,160000)
    alljet_n = TH1F("jet_pt","      ;[GeV] ;Number of Events",100,900,2400)
    jet_jvf= TH1F("jet_jvf","  ;[GeV] ;Number of Events",200,0,240000)
    lep_E  = TH1F("lep_E","      ;[GeV] ;Number of Events",200,0,240000)
    jet_eta= TH1F("jet_eta","       ;[GeV] ;Number of Events",200,0,240000) 
 

    #Reconstructed objects Di                   Note: all recon. objects will be in letters when filling histos 
    di_jets= TH1F("di_jets"," ;Dijet Mass [GeV] ;Number of Events",60, 0, 600000)                  #B

#This is the body of the analysis code. Here you will find cuts, fills, reconstructed event kinematics, TLorentz Vectors, etc. 
#TLV for H->bb candidate jets from 2018publ. and transverse momentum  
    jet1 = TLorentzVector()                                     # TLV is a four vector class used to declare a variable  
    jet2 = TLorentzVector()                                     # that describes position and time or momentum and energy. 

#Starting a loop for all entries in the mini branch. Here you can easily pull the object of interest from the tree to apply cuts and fills for histograms  
    for evt in range(nEntries):
        tree_pull.GetEntry(evt) 

#Applying Cuts relative to H-> yybb production. 
    if (tree_pull.jet_n < 4): continue                      # Jets ------> interested in number of jets < 4 to  

        
#Here we have a special cut. "explain here". 
#Note: This regime must go in the bottom of the body of code or else it will interfere with some of the data fills in histogram. 
            
#Simple Fills for histograms of objects with loops 
    for evt in range(nEntries):
        tree_pull.GetEntry(evt)
        #Simple Fills for histograms of objects with loops 
        jet_n.Fill(tree_pull.jet_n)                                        #3
        lep_n.Fill(tree_pull.lep_n)
        met_et.Fill(tree_pull.met_et) 
        met_phi.Fill(tree_pull.met_phi) 
        alljet_n.Fill(tree_pull.alljet_n) 
        
    for i in range(tree_pull.jet_n):
        jet_pt.Fill(tree_pull.jet_pt[i])
    for j in range(tree_pull.lep_n):
        lep_pt.Fill(tree_pull.lep_pt[j]) 
    for k in range(tree_pull.lep_n):
        lep_E.Fill(tree_pull.lep_E[k]) 

        
#Reference to TLV, we now use cylindrical coordinates DiJets and divide by 1000 to stay in propper units [GeV]
        jet1.SetPtEtaPhiM(tree_pull.jet_pt[0],tree_pull.jet_eta[0], tree_pull.jet_phi[0], tree_pull.jet_m[0])
        jet2.SetPtEtaPhiM(tree_pull.jet_pt[1],tree_pull.jet_eta[1], tree_pull.jet_phi[1], tree_pull.jet_m[1])

#Next, we conduct basic kinematics to fill the reconstructed variables 
        dijet_v = (jet1 + jet2)
        dijet_mag = dijet_v.Mag() 
        di_jets.Fill(dijet_mag)                               #B 
   
    outputfile = TFile('plots_'+datasample+'_'+str(d)+'.root', 'recreate') 

    #write the histograms into ROOT file 
    lep_n.Write()
    lep_pt.Write()                 
    lep_eta.Write()
    lep_phi.Write()
    met_et.Write()
    met_phi.Write()
    jet_n.Write()
    jet_pt.Write()
#    alljet_n.Write()
    jet_jvf.Write()
    lep_E.Write()
#    di_jets.Write()
    jet_eta.Write()
    
    
    outputfile.cd()
    outputfile.Write() 
    outputfile.Close()
    
    rootFile.Close() 


#Calling the analyze function to run on the 280 GeV and/or 300GeV signal sample with background 
analyze("WW_Boson", "/eos/user/n/nnunez/mc_105985.WW.root")
analyze("VBF_Boson", "/eos/user/n/nnunez/mc_161055.VBFH125_WW2lep.root")
analyze("GG_Boson", "/eos/user/n/nnunez/mc_161005.ggH125_WW2lep.root")


d = date.today()                       
BackgroundFile = TFile("plots_WW_Boson_"+str(d)+".root") 
SignalFile = TFile("plots_VBF_Boson_"+str(d)+".root") 
SignalFile280 = TFile("plots_GG_Boson_"+str(d)+".root")             

#FileWW.GetListOfKeys().Print()

#Function uses the signal and background files, gets the histograms stored inside
# and creates the histograms into dictionaries 
def Dictionaries(SignalFile, BackgroundFile): #analyze 
    dictionS = dict() 
    dictionSS = dict() 
    dictionB = dict() 
    for tkey in SignalFile.GetListOfKeys():
        Histo_Name = tkey.GetName()    
        HistS = SignalFile.Get(Histo_Name)            
        dictionS[Histo_Name] = HistS 
        HistB = BackgroundFile.Get(Histo_Name) 
        dictionB[Histo_Name] = HistB 
        HistSS = SignalFileWW.Get(Histo_Name)
        dictionSS[Histo_Name] = HistSS
    return dictionS, dictionB,dictionSS 

#The return dictionaries are passed to this function but needs to be renamed to avoid syntax 
#errors. dictionS becomes updatedictionS and dictionB becomes updatedictionB
#This function only creates stacked histograms/dictionaries from the signal and background 
#As well as the color for the dictionaries. We are not drawing anything here 
def Stacking(updatedictionS, updatedictionB, updatedictionSS):
    color = {} 
    color["Signal"] = ROOT.kRed-3
    color["Background"] = ROOT.kBlue+1
    color["SignalWW"] = ROOT.kViolet-5
    stack = {}
    for sb in updatedictionS:
        stack[sb] = THStack("stacking_%s" % (sb), "stacking_%s" % (sb)) 
        Hist1 = updatedictionS[sb] 
        Hist1.SetLineColor(color["Signal"]) 
        stack[sb].Add(Hist1) 
        Hist2 = updatedictionB[sb] 
        Hist2.SetLineColor(color["Background"])
        stack[sb].Add(Hist2)
        Hist3 = updatedictionSS[sb] 
        Hist3.SetLineColor(color["SignalWW"])
        stack[sb].Add(Hist3)
    return stack 

#The return stack runs all the variables in the dictionaries which passes to this function
#Stack is now called stack_Hist and creates the frame of canvases 
def canvas(stack_Hist):
    can = {} 
    for c in stack_Hist: 
        can[c] = TCanvas("Canvas_%s" % (c),"Canvas_%s" % (c),600,500)
    return can

def callingfunctions(SignalFile, BackgroundFile):                  # SignalFile, BackgroundFile
    sig_dict, bac_dict, sig_dict280 = Dictionaries(SignalFile, BackgroundFile) #     
    stacks = Stacking(sig_dict,bac_dict, sig_dict280)                          # updatedictionS, updatedictionB 
    canvases = canvas(sig_dict)    

    for H in sig_dict:
        canvases[H].cd()  
        stacks[H].Draw()
        legend = TLegend(0.50,0.70,0.90,0.90) 
        legend.AddEntry(sig_dict[H],"VBF_Boson","l")
        legend.AddEntry(bac_dict[H],"GG_Boson","l") # bac_dict[H].GetName()
        legend.AddEntry(bac_dict[H],"WW_Boson","l") # bac_dict[H].GetName()

        legend.Draw()  
        stacks[H].GetXaxis().SetTitle("[GeV]") 
        stacks[H].GetYaxis().SetTitle("Number of Events")
        canvases[H].Update()
        canvases[H].SaveAs("%s_plot.pdf"%(H))

        functions = callingfunctions(SignalFile, BackgroundFile) 
